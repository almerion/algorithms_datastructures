#include "random.h"
#include <unistd.h>

int rand_number(int min_num, int max_num)
{
  sleep(1);

  int rand_num = 0;
  srand(time(NULL));
  rand_num = (rand() % (max_num - min_num));

  return rand_num;
}
