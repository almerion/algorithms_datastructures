#include <stdio.h>
#include "merge.h"
#include "../lib/printarr.h"

int main()
{
  int arr[] = {5, 9, 2, 1, 6, 4, 7, 3, 8, 0};
  
  print_array(arr, 9);
  merge(arr, 0, 4, 9);
  print_array(arr, 9);

  return 0;
  
}
