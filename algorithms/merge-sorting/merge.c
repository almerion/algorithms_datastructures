#include <stdio.h>
#include <limits.h>
#include "../lib/printarr.h"

/* first_index <= second_index < last_index *
 * right_arr[p..q]                          *
 * left_arr[q+1..r                          *
 * mainarr[p..r]                            *
 * p = first_index                          *
 * q = second_index                         *
 * r = last_index                           *
 *------------------------------------------*
 * for example:                             *
 * mainarr[9]                               *
 * right_arr[0..4]                          *
 * left_arr[5..9]                           */
void merge(int arr[], int first_index, int second_index, int last_index)
{

  int max_value = INT_MAX;
  int len_arr_right; /* indicates the length of rigth subarray */
  int len_arr_left; /* indicates the length of left subarray */
  
  len_arr_right = second_index - first_index + 1;
  len_arr_left = last_index - second_index;

  int arr_right[len_arr_right];
  int arr_left[len_arr_left];
  int i, j, k; /* loop counters */
  
  for (i = 1; i <= len_arr_right; i++)
    arr_right[i - 1] = arr[first_index + i - 1];

  print_array(arr_right, 4);

  for (j = 1; j <= len_arr_left; j++)
    arr_left[j - 1] = arr[second_index + j];

  print_array(arr_left, 4);
  arr_right[len_arr_right + 1] = max_value; /* BUG: max value doesn't add to this array */
  arr_left[len_arr_left + 1] = max_value; /* BUG: max value doesn't add to this array */

  j = 1;
  i = 1;
  for (k = first_index; k <= last_index; k++)
    {
      if (arr_right[i - 1] <= arr_left[j])
	{
	  arr[k] = arr_right[i - 1];
	  i++;
	}
      else
	{
	  arr[k] = arr_left[j];
	  j++;
	}
    }
  
}
