#include <stdio.h>

#define ARR_LENGTH(_array) (sizeof(_array) / sizeof(_array[0]))

int main()
{
  int j, i, key;
  int arr[] = {5, 6, 3, 4, 1, 0, 2};

  i, key = 0;
  j = 1;
    while (j < ARR_LENGTH(arr))
      {
	j++;
	key = arr[j];
	i = j - 1;
	while (i >= 0 && arr[i] > key)
	  {
	    arr[i+1] = arr[i];
	    i--;
	  }
	arr[i+1] = key;
      }


  for (i = 0; i < ARR_LENGTH(arr); i++)
    {
      printf("arr[%d] --> %d\n", i, arr[i]);
    }

  return 0;
}
