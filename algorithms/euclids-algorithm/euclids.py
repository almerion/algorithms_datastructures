 # Euclid's Algorithm:                     
 # This algorithm finds greatest positive  
 # common integer between two integer and  
 # prints this integer(s) to screen.

num1 = int(input("please enter first number: "))
num2 = int(input("please enter second number: "))

print("Common divisors of {0} and {1}: \n".format(num1, num2))

i = 1;
while (i <= num1 or i <= num2):
    if(num1 % i == 0 and num2 % i == 0):
        print(i)
        i = i + 1
    else:
        i = i + 1
        
