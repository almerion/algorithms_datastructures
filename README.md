# Algorithms & Data Structures
---
This repo contains works about algorithms and data structures that written in C and Python.

### Algorithms

* Euclid's Algorithm
  * [C](euclids-algorithm/euclids.c)
  * [Python](euclids-algorithm/euclids.py)
* Merge Sorting
  * [C](merge-sorting/merge.c)
* Insertion Sort
  * [C](insertion-sort.c)
  * [Python](insertion-sort.py)
* Recursive
  * [C](recursive/fact.c)
* Tail Recursive
  * [C](tail-recursive/facttail.c)

### Data Structures

  *Singly Linked List
    * [C](data_structures/singly_linked_list/simplelist.c)
